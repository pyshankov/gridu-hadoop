plugins {
    java
}

group = GriduHadoopDeps.extendGroup("hive-udf")
version = GriduHadoopDeps.version

repositories {
    mavenCentral()
    maven { url = uri("https://repo.hortonworks.com/content/repositories/releases") }
    maven { url = uri("https://repo.spring.io/plugins-release") }
}

dependencies {
    implementation(project(":common"))
    compileOnly(GriduHadoopDeps.Dependencies.hadoopClient)
    compileOnly(GriduHadoopDeps.Dependencies.hiveExec)

    testImplementation(GriduHadoopDeps.Dependencies.hadoopClient)
    testImplementation(GriduHadoopDeps.Dependencies.hiveExec)
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.register<Jar>("fatJar") {
    from(Callable { configurations["runtimeClasspath"].map { if (it.isDirectory) it else zipTree(it) } })
    with(tasks["jar"] as CopySpec)
}