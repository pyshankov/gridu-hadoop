package com.griddynamics.pmarchenko.labs.hadoop.udf;

import com.griddynamics.pmarchenko.labs.hadoop.common.IpAddressMatcher;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorConverter;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.BooleanWritable;

public class IPWithinNetworkMaskUDF extends GenericUDF {

    private final static Integer PARAMS_CNT = 2;
    private final static String UDF_NAME = "com.griddynamics.pmarchenko.labs.udf.IPWithinNetworkMaskUDF";

    private transient PrimitiveObjectInspectorConverter.TextConverter ipAddressConverter;
    private transient PrimitiveObjectInspectorConverter.TextConverter networkMaskConverter;

    private final BooleanWritable result = new BooleanWritable();

    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        if (arguments.length != PARAMS_CNT){
            throw new UDFArgumentException(UDF_NAME + " requires " + PARAMS_CNT + " value argument. Found :" + arguments.length);
        }
        checkParamCategory(arguments[0]);
        checkParamCategory(arguments[1]);
        ipAddressConverter = new PrimitiveObjectInspectorConverter.TextConverter((PrimitiveObjectInspector) arguments[0]);
        networkMaskConverter = new PrimitiveObjectInspectorConverter.TextConverter((PrimitiveObjectInspector) arguments[1]);
        return PrimitiveObjectInspectorFactory.writableBooleanObjectInspector;
    }

    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        Object ipAddressObject = arguments[0].get();
        Object networkMaskObject = arguments[1].get();
        if (ipAddressObject == null && networkMaskObject == null) {
            result.set(true);
            return result;
        } else if (ipAddressObject == null || networkMaskObject == null) {
            result.set(false);
            return result;
        }
        String ipAddress = (ipAddressConverter.convert(ipAddressObject)).toString();
        String networkMask = (networkMaskConverter.convert(networkMaskObject)).toString();
        if (ipAddress == null && networkMask == null) {
            result.set(true);
            return result;
        } else if (ipAddress == null || networkMask == null) {
            result.set(false);
            return result;
        }

        result.set(IpAddressMatcher.isIpWithinMask(ipAddress, networkMask));

        return result;
    }

    @Override
    public String getDisplayString(String[] children) {
        return "Check whether an IP belongs to a particular network. IP - parameter 1, network - parameter 2.\n" +
                "Result: true -> IP belongs to network, false IP doesn't belongs to network";
    }

    private static void checkParamCategory(ObjectInspector argumentIO) throws UDFArgumentException {
        if(argumentIO.getCategory() != ObjectInspector.Category.PRIMITIVE ){
            throw new UDFArgumentException(UDF_NAME + " takes only primitive types. found "
                    + argumentIO.getTypeName());
        }
        PrimitiveObjectInspector argument = (PrimitiveObjectInspector) argumentIO;
        switch (argument.getPrimitiveCategory()) {
            case STRING:
            case CHAR:
            case VARCHAR:
                break;
            default:
                throw new UDFArgumentException(
                  UDF_NAME + " takes only STRING/CHAR/VARCHAR types. Found " + argument.getPrimitiveCategory());
        }
    }



}
