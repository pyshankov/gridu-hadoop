package com.griddynamics.pmarchenko.labs.hadoop.udf;

import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.WritableBooleanObjectInspector;

class IPWithinNetworkMaskUDFTest {


    public static void main(String[] args) throws Exception {
        IPWithinNetworkMaskUDF udf = new IPWithinNetworkMaskUDF();

        ObjectInspector ipAddress = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
        ObjectInspector networkMask = PrimitiveObjectInspectorFactory.javaStringObjectInspector;

        WritableBooleanObjectInspector resultInspector =
                (WritableBooleanObjectInspector) udf.initialize(new ObjectInspector[]{ipAddress, networkMask});


        Object result = udf.evaluate(
                new GenericUDF.DeferredObject[] {
                        new GenericUDF.DeferredJavaObject("192.168.1.3"),
                        new GenericUDF.DeferredJavaObject("192.168.1.0/30"),
                });

        boolean res = resultInspector.get(result);

        System.out.println(res);
    }


}