package com.griddynamics.pmarchenko.labs.hadoop.generator

import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalTime}
import java.util.concurrent.ThreadLocalRandom

import com.github.javafaker.Faker
import org.apache.commons.math3.distribution.NormalDistribution

case class Event(productName: String, productPrice: Double, purchaseDate: Timestamp,
                 productCategory: String, clientIP: String)

class EventCsvMapper(val delimiter: String) {
  final val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
  def toCsv(event: Event): String = {
    Array(
      event.productName,
      event.productPrice,
      event.purchaseDate.toLocalDateTime.format(DATE_TIME_FORMATTER),
      event.productCategory,
      event.clientIP
    ).mkString(delimiter)
  }
}

class EventGenerator(val startDate: LocalDate, val endDate: LocalDate){

  private val dates: List[LocalDate] = EventGenerator.getDatesWithinRange(this.startDate,this.endDate)

  def generateEvent(): Event = {
    val (category,product): (String,String) = EventGenerator.randomCategoryAndProduct()
    val randomDate: LocalDate = this.dates(ThreadLocalRandom.current().nextInt(0,this.dates.size))
    val randomDateTime: Timestamp = Timestamp.valueOf(randomDate.atTime(EventGenerator.normallyDistributedRandomTime()))
    val randomPrice: Int = EventGenerator.normallyDistributedRandomPrice()
    val randomIp: String = EventGenerator.FAKER.internet().ipV4Address()
    Event(product, randomPrice, randomDateTime, category, randomIp)
  }
}
object EventGenerator {
  private final val FAKER: Faker = new Faker
  private final val NORMAL_DISTRIBUTION: NormalDistribution = new NormalDistribution
  private final val SECONDS_IN_DAY: Int = 86400
  private final val MAX_PRODUCT_PRICE: Int = 1000
  private final val PRODUCTS_CATEGORIES: Array[(String, Array[String])] = Array(
    ("phones", Array("iphone 11", "meizu mx6", "xiaomi x9", "nokia 1100", "Vertu signature", "Vertu signature", "Samsung Galaxy", "Samsung s1", "Samsung s2", "Samsung s3", "Samsung s4", "Samsung s5", "Samsung s8", "huawei 100")),
    ("laptops", Array( "mac book pro 15", "dell cx40", "asus znote", "huawei 100x", "mac book air", "asus zenbook", "asus dx450", "aser 100", "aser 2120", "dell 20", "fujitsu pretty", "fujitsu 100x")),
    ("headphones", Array( "airpods", "sennheiser", "asus znote", "sennheiser 1", "sennheiser 2", "sennheiser 3", "sennheiser 4", "sennheiser 5", "sennheiser 6", "sennheiser 7", "sennheiser 8"))
  )
  // time within a range og seconds [0, 86400]
  private def normallyDistributedRandomTime(): LocalTime =
    LocalTime.ofSecondOfDay((NORMAL_DISTRIBUTION.cumulativeProbability(NORMAL_DISTRIBUTION.sample()) * SECONDS_IN_DAY).toLong)

  private def normallyDistributedRandomPrice(): Int =
    (NORMAL_DISTRIBUTION.cumulativeProbability(NORMAL_DISTRIBUTION.sample()) * MAX_PRODUCT_PRICE).toInt

  private def randomCategoryAndProduct(): (String, String) = {
    val randomCategoryNumber =  ThreadLocalRandom.current().nextInt(0, PRODUCTS_CATEGORIES.length)
    val randomProductNumber = ThreadLocalRandom.current().nextInt(0,PRODUCTS_CATEGORIES(randomCategoryNumber)._2.length)
    (PRODUCTS_CATEGORIES(randomCategoryNumber)._1, PRODUCTS_CATEGORIES(randomCategoryNumber)._2(randomProductNumber))
  }
  private def getDatesWithinRange(startDateInclusive: LocalDate, endDateInclusive: LocalDate): List[LocalDate] = {
    val end = endDateInclusive.plusDays(1).toEpochDay
    val start = startDateInclusive.toEpochDay
    if (end < start) throw new IllegalArgumentException(endDateInclusive + " < " + startDateInclusive)
    List.range(start, end).map(LocalDate.ofEpochDay)
  }
}
