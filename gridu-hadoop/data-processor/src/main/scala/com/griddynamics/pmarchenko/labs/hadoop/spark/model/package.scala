package com.griddynamics.pmarchenko.labs.hadoop.spark

import java.sql.Timestamp
import java.time.format.DateTimeFormatter

import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType, TimestampType}

package object model {

  case class ProductPurchaseEvent(productName: String, productPrice: Double, purchaseDate: Timestamp,
                                   productCategory: String, clientIP: String)
  object ProductPurchaseEvent {
    final val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    final val productName: String = "productName"
    final val productPrice: String = "productPrice"
    final val purchaseDate: String = "purchaseDate"
    final val productCategory: String = "productCategory"
    final val clientIP: String = "clientIP"

    final val SCHEME = StructType(Array(
      StructField(ProductPurchaseEvent.productName, StringType),
      StructField(ProductPurchaseEvent.productPrice, DoubleType),
      StructField(ProductPurchaseEvent.purchaseDate, TimestampType),
      StructField(ProductPurchaseEvent.productCategory, StringType),
      StructField(ProductPurchaseEvent.clientIP, StringType)
    ))

    def mapRowToObj(row: Row): ProductPurchaseEvent =
      ProductPurchaseEvent(
        row.getAs[String](productName),
        row.getAs[Double](productPrice),
        row.getAs[Timestamp](purchaseDate),
        row.getAs[String](productCategory),
        row.getAs[String](clientIP)
      )
  }

  case class CountryIp4(network: String, geonameId: Int, registeredCountryGeonameId: Int, representedCountryGeonameId: Int,
                        isAnonymousProxy: Boolean, isSatelliteProvider: Boolean)
  object CountryIp4 {
    def mapRowToObj(row: Row): CountryIp4 =
      CountryIp4(
        row.getAs[String]("network"),
        row.getAs[Int]("geoname_id"),
        row.getAs[Int]("registered_country_geoname_id"),
        row.getAs[Int]("represented_country_geoname_id"),
        if (row.getAs[Int]("is_anonymous_proxy").equals(1)) true else false,
        if (row.getAs[Int]("is_satellite_provider").equals(1)) true else false
      )
  }

  case class CountryLocation(geonameId: Int, localeCode: String, continentCode: String, continentName: String,
                             countryIsoCode: String, countryName: String, isInEuropeanUnion: Boolean)
  object CountryLocation {
    def mapRowToObj(row: Row): CountryLocation =
      CountryLocation(
        row.getAs[Int]("geoname_id"),
        row.getAs[String]("locale_code"),
        row.getAs[String]("continent_code"),
        row.getAs[String]("continent_name"),
        row.getAs[String]("country_iso_code"),
        row.getAs[String]("country_name"),
        if (row.getAs[Int]("is_in_european_union").equals(1)) true else false
      )
  }


  case class CategoryInfo(productCategory: String, purchasesCount: Long)
  object CategoryInfo {
    final val productCategory: String = "productCategory"
    final val purchasesCount: String = "purchasesCount"

    def mapRowToObj(row: Row): CategoryInfo = CategoryInfo(
      row.getAs[String](productCategory),
      row.getAs[Long](purchasesCount)
    )
  }

  case class ProductCategoryInfo(productCategory: String, productName: String, purchasesCount: Long)
  object ProductCategoryInfo {
    final val productCategory: String = "productCategory"
    final val productName: String = "productName"
    final val purchasesCount: String = "purchasesCount"

    def mapRowToObj(row: Row): ProductCategoryInfo = ProductCategoryInfo(
      row.getAs[String](productCategory),
      row.getAs[String](productName),
      row.getAs[Long](purchasesCount)
    )
  }

  case class CountrySpending(countryName: String, amount: Double)
  object CountrySpending {
    final val country: String = "country"
    final val amount: String = "amount"

    def mapRowToObj(row: Row): CountrySpending = CountrySpending(
      row.getAs[String](country),
      row.getAs[Double](amount)
    )
  }
}
