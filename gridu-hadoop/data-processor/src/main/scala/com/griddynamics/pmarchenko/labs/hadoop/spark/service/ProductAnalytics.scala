package com.griddynamics.pmarchenko.labs.hadoop.spark.service

import com.griddynamics.pmarchenko.labs.hadoop.spark.model.{CategoryInfo, CountryIp4, CountryLocation, CountrySpending, ProductCategoryInfo, ProductPurchaseEvent}
import org.apache.spark.sql.Dataset

object ApiType extends Enumeration {
  type ApiType = Value
  val Rdd, Dataset, Other = Value
  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}

trait ProductAnalytics {

  def topNCategories(events: Dataset[ProductPurchaseEvent], n: Int) : Dataset[CategoryInfo]

  def topNProductsWithinCategory(events: Dataset[ProductPurchaseEvent], n: Int) : Dataset[ProductCategoryInfo]

  def topNCountriesByMoneySpending(events: Dataset[ProductPurchaseEvent], countryIp4: Dataset[CountryIp4], countryLocation: Dataset[CountryLocation], n: Int): Dataset[CountrySpending]

}
