package com.griddynamics.pmarchenko.labs.hadoop.spark

import org.apache.spark.sql.Dataset

class SparkDbWriter(val jdbcUrl: String, val driver: String, val user: String,
 val password: String, val table: String, val saveMode: String) {

  def writeDfToDb(dataset: Dataset[_]): Unit = {
    val prop = new java.util.Properties
    prop.setProperty("driver", driver)
    prop.setProperty("user", user)
    prop.setProperty("password", password)

    dataset.write.mode(saveMode)
      .jdbc(jdbcUrl, table, prop)
  }
}
