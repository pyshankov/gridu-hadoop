package com.griddynamics.pmarchenko.labs.hadoop.spark.service

import com.griddynamics.pmarchenko.labs.hadoop.common.IpAddressMatcher
import com.griddynamics.pmarchenko.labs.hadoop.spark.model.{CategoryInfo, CountryIp4, CountryLocation, CountrySpending, ProductCategoryInfo, ProductPurchaseEvent}
import com.griddynamics.pmarchenko.labs.hadoop.spark.util.{BoundedPriorityQueue => BPQ}
import org.apache.spark.sql.{Dataset, Encoders, SQLContext}
import org.apache.spark.rdd.RDD

import scala.reflect.runtime.universe.TypeTag



class ProductAnalyticsRddApi(sqlContext: SQLContext) extends ProductAnalytics {

  override def topNCategories(events: Dataset[ProductPurchaseEvent], n: Int): Dataset[CategoryInfo] = rddToDs {
      events.rdd.map(p => (p.productCategory, 1))
        .reduceByKey(_ + _).map(v => CategoryInfo(v._1, v._2))
        .sortBy(_.purchasesCount, false)
    }.limit(n)

  override def topNProductsWithinCategory(events: Dataset[ProductPurchaseEvent], n: Int): Dataset[ProductCategoryInfo] = rddToDs {
      val ordering: Ordering[ProductCategoryInfo] = Ordering.by(_.purchasesCount)
      val merge = (q1: BPQ[ProductCategoryInfo], q2: BPQ[ProductCategoryInfo]) => q1 ++= q2
      val add = (q: BPQ[ProductCategoryInfo], p: ProductCategoryInfo) => q += p

      events.rdd.map(ppe => ((ppe.productCategory,ppe.productName),1))
      .reduceByKey(_ + _).map(v => ProductCategoryInfo(v._1._1, v._1._2, v._2))
      .keyBy(_.productCategory)
      .aggregateByKey(new BPQ[ProductCategoryInfo](n)(ordering))(add,merge)
      .flatMapValues(_.toList)
      .values.sortBy(v => (v.productCategory, v.purchasesCount, v.productName), false)
  }

  override def topNCountriesByMoneySpending(events: Dataset[ProductPurchaseEvent], countryIp4: Dataset[CountryIp4],
                                            countryLocation: Dataset[CountryLocation], n: Int): Dataset[CountrySpending] = rddToDs {
    val countryLocationRdd: RDD[(Int,String)] = countryLocation.rdd.map(cL => (cL.geonameId, cL.countryName))
    val countryIp4Rdd: RDD[(Int,String)] = countryIp4.rdd.map(cIp => (cIp.geonameId, cIp.network))
    val eventsRdd: RDD[(String,Double)] = events.rdd.map(e => (e.clientIP, e.productPrice))

    val countryNetworkJoinedRdd: RDD[(String,String)] = countryLocationRdd.join(countryIp4Rdd).map(v => (v._2._1, v._2._2)).persist()

    eventsRdd.cartesian(countryNetworkJoinedRdd)
      .filter(p => IpAddressMatcher.isIpWithinMask(p._1._1, p._2._2))
      .map(p => (p._2._1, p._1._2))
      .reduceByKey(_ + _)
      .map(v => CountrySpending(v._1, v._2))
      .sortBy(_.amount, false)
  }.limit(n)

  private def rddToDs[T <: Product : TypeTag](rdd: RDD[T]): Dataset[T] = sqlContext.createDataset(rdd)(Encoders.product[T])

}
