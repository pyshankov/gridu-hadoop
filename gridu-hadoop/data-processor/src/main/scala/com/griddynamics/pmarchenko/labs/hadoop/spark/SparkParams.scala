package com.griddynamics.pmarchenko.labs.hadoop.spark

import com.griddynamics.pmarchenko.labs.hadoop.spark
import com.griddynamics.pmarchenko.labs.hadoop.spark.service.ApiType
import com.griddynamics.pmarchenko.labs.hadoop.spark.JobType.JobType
import com.griddynamics.pmarchenko.labs.hadoop.spark.service.ApiType
import com.griddynamics.pmarchenko.labs.hadoop.spark.service.ApiType.ApiType

object JobType extends Enumeration {
  type JobType = Value
  val Top10Categories, Top10ProductsInCategory, Top10SpendingCountries, Other = Value
  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}

case class SparkParams(
                        eventsDir: String,
                        ip4NetworkMasks: String,
                        countryLocations: String,
                        jobType: JobType,
                        targetJdbcUrl: String,
                        targetUser: String,
                        targetPassword: String,
                        targetTable: String,
                        jdbcDriver: String,
                        apiType: ApiType
                      ){
  def toArgs(): Array[String] = {
    val array: Array[String] = new Array[String](10)
    array(0) = eventsDir
    array(1) = ip4NetworkMasks
    array(2) = countryLocations
    array(3) = jobType.toString
    array(4) = targetJdbcUrl
    array(5) = targetUser
    array(6) = targetPassword
    array(7) = targetTable
    array(8) = jdbcDriver
    array(10) = apiType.toString
    array
  }
}
object SparkParams {
  private final val PARAMS_COUNT = 10
  def fromArgs(str: Array[String]): SparkParams = {
    if (str.size != PARAMS_COUNT)
      throw new IllegalArgumentException("Invalid params count: " + str.size + " required: " + PARAMS_COUNT)
    else spark.SparkParams(
      str(0),
      str(1),
      str(2),
      JobType.withNameOpt(str(3)).getOrElse(JobType.Other),
      str(4),
      str(5),
      str(6),
      str(7),
      str(8),
      ApiType.withNameOpt(str(9)).getOrElse(ApiType.Other)
    )
  }
}

