package com.griddynamics.pmarchenko.labs.hadoop.generator

import java.time.LocalDate
import java.time.format.DateTimeFormatter

//localhost 3333 2020-01-01 2020-01-07 10000 ,
case class StreamParams(socketHost: String, socketPort: Integer, dayStart: LocalDate, dayEnd: LocalDate, recordsNumber: Long, delimiter: String) {
  def toArgs(): Array[String] = {
    val array: Array[String] = new Array[String](4)
    array(0) = socketHost.toString
    array(1) = socketPort.toString
    array(2) = StreamParams.DATE_FORMATTER.format(this.dayStart)
    array(3) = StreamParams.DATE_FORMATTER.format(this.dayEnd)
    array(4) = recordsNumber.toString
    array(5) = delimiter
    array
  }
}

object StreamParams {
  final val DATE_FORMATTER: DateTimeFormatter = DateTimeFormatter.ISO_DATE
  def fromArgs(str: Array[String]): StreamParams =
    StreamParams(
      str(0).toString,
      str(1).toInt,
      LocalDate.parse(str(2), StreamParams.DATE_FORMATTER),
      LocalDate.parse(str(3), StreamParams.DATE_FORMATTER),
      str(4).toLong,
      str(5)
    )
}