package com.griddynamics.pmarchenko.labs.hadoop.spark


import com.griddynamics.pmarchenko.labs.hadoop.spark.model.{CountryIp4, CountryLocation, ProductPurchaseEvent}
import com.griddynamics.pmarchenko.labs.hadoop.spark.service.ApiType.{Dataset, Other, Rdd}
import com.griddynamics.pmarchenko.labs.hadoop.spark.service.{ProductAnalytics, ProductAnalyticsDatasetApi, ProductAnalyticsRddApi}
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import org.apache.spark.sql.functions.{col}

// params:
// gridu-hadoop/data-processor/src/main/resources/events/*/*/*
// gridu-hadoop/data-processor/geo-data/GeoLite2-Country-Blocks-IPv4.csv
// gridu-hadoop/data-processor/geo-data/GeoLite2-Country-Locations-en.csv
// Top10Categories
// jdbc:postgresql://127.0.0.1/lab_gridu_hadoop
// ambari
// bigdata
// top_10_money_spending_countries
// org.postgresql.Driver
// Rdd

object SparkMain {

  def main(args: Array[String]): Unit = {

    val params: SparkParams = SparkParams.fromArgs(args)

    val spark: SparkSession = SparkSession.builder()
//      .master("local[*]")
      .getOrCreate()
//    spark.sparkContext.setLogLevel("ERROR")

    val productAnalytics: ProductAnalytics = params.apiType match {
      case Rdd => new ProductAnalyticsRddApi(spark.sqlContext)
      case Dataset => new ProductAnalyticsDatasetApi
      case Other => throw new IllegalArgumentException("invalid param 'apiType'")
    }

    val events: Dataset[ProductPurchaseEvent] = eventsDs(spark, params)
    var result: Dataset[_] = null

    params.jobType match {
      case JobType.Top10SpendingCountries =>
        result = productAnalytics.topNCountriesByMoneySpending(events, countryIp4Ds(spark, params), countryLocationDs(spark, params), 10)
      case JobType.Top10Categories =>
        result = productAnalytics.topNCategories(events, 10)
      case JobType.Top10ProductsInCategory =>
        result = productAnalytics.topNProductsWithinCategory(events, 10)
      case JobType.Other =>
        throw new IllegalArgumentException("invalid param 'jobType'")
    }

    result.show(100)
    new SparkDbWriter(params.targetJdbcUrl, params.jdbcDriver, params.targetUser,
      params.targetPassword, params.targetTable, "overwrite")
      .writeDfToDb(result)

    spark.stop()
  }

  def countryIp4Ds(spark: SparkSession, params: SparkParams): Dataset[CountryIp4] =
    spark.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(params.ip4NetworkMasks)
      .map(CountryIp4.mapRowToObj)(Encoders.product[CountryIp4])
      .repartition(col("geonameId"))

  def countryLocationDs(spark: SparkSession, params: SparkParams): Dataset[CountryLocation] =
    spark.read.format("csv")
    .option("delimiter", ",")
    .option("header", "true")
    .option("inferSchema", "true")
    .load(params.countryLocations)
    .map(CountryLocation.mapRowToObj)(Encoders.product[CountryLocation])
      .repartition(col("geonameId"))

  def eventsDs(spark: SparkSession, params: SparkParams): Dataset[ProductPurchaseEvent] =
    spark.read.format("csv")
      .option("delimiter", ",")
      .schema(ProductPurchaseEvent.SCHEME)
      .load(params.eventsDir)
      .map(ProductPurchaseEvent.mapRowToObj)(Encoders.product[ProductPurchaseEvent])

}
