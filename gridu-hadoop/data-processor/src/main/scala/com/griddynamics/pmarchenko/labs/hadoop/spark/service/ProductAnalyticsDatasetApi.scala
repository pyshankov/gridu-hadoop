package com.griddynamics.pmarchenko.labs.hadoop.spark.service

import com.griddynamics.pmarchenko.labs.hadoop.common.IpAddressMatcher
import com.griddynamics.pmarchenko.labs.hadoop.spark.model.{CategoryInfo, CountryIp4, CountryLocation, CountrySpending, ProductCategoryInfo, ProductPurchaseEvent}
import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.{Dataset, Encoders}
import org.apache.spark.sql.functions.{col, count, desc, rank, udf}


class ProductAnalyticsDatasetApi extends ProductAnalytics {

  private val isIpWithinMask: UserDefinedFunction = udf((ip:String, mask: String) => IpAddressMatcher.isIpWithinMask(ip,mask))

  override def topNCategories(events: Dataset[ProductPurchaseEvent], n: Int): Dataset[CategoryInfo] =
    events.groupBy(ProductPurchaseEvent.productCategory)
      .agg(count(ProductPurchaseEvent.productName))
      .toDF(CategoryInfo.productCategory, CategoryInfo.purchasesCount)
      .sort(desc(CategoryInfo.purchasesCount))
      .limit(n)
      .map(CategoryInfo.mapRowToObj)(Encoders.product[CategoryInfo])

  override def topNProductsWithinCategory(events: Dataset[ProductPurchaseEvent], n: Int): Dataset[ProductCategoryInfo] = {
    val overProductCategory = Window.partitionBy(ProductPurchaseEvent.productCategory)
      .orderBy(col(ProductCategoryInfo.purchasesCount).desc, col(ProductPurchaseEvent.productName).desc)

    events.groupBy(ProductPurchaseEvent.productCategory, ProductPurchaseEvent.productName)
      .agg(count(ProductPurchaseEvent.productName))
      .toDF(ProductCategoryInfo.productCategory, ProductCategoryInfo.productName, ProductCategoryInfo.purchasesCount)
      .withColumn("rank", rank().over(overProductCategory))
      .filter("rank <= " + n)
      .map(ProductCategoryInfo.mapRowToObj)(Encoders.product[ProductCategoryInfo])
  }

  override def topNCountriesByMoneySpending(
                                             events: Dataset[ProductPurchaseEvent],
                                             countryIp4: Dataset[CountryIp4],
                                             countryLocation: Dataset[CountryLocation],
                                             n: Int): Dataset[CountrySpending] = {

    val joined = countryIp4.join(countryLocation, "geonameId")
      .select("network", "countryName")

    joined.join(events, isIpWithinMask(events(ProductPurchaseEvent.clientIP), joined("network")) === true)
      .select("countryName", ProductPurchaseEvent.productPrice)
      .groupBy("countryName")
      .sum(ProductPurchaseEvent.productPrice)
      .toDF(CountrySpending.country, CountrySpending.amount)
      .orderBy(col(CountrySpending.amount).desc)
      .limit(n)
      .map(CountrySpending.mapRowToObj)(Encoders.product[CountrySpending])

  }


}
