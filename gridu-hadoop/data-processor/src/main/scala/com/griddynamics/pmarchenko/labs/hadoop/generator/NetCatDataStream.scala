package com.griddynamics.pmarchenko.labs.hadoop.generator

import java.io.{BufferedReader, IOException, InputStreamReader, PrintWriter}
import java.net.Socket
import java.time.LocalDate

object NetCatDataStream {

    def main(args: Array[String]): Unit = {

      val params: StreamParams =
        if (args.length == 0)
          StreamParams(
            "sandbox-hdp.hortonworks.com",
            60080,
            LocalDate.of(2020,1,1),
            LocalDate.of(2020,1,7),
            1000 ,
            ",")
        else StreamParams.fromArgs(args)

    val eventGenerator: EventGenerator = new EventGenerator(params.dayStart, params.dayEnd)
    val eventCSVMapper: EventCsvMapper = new EventCsvMapper(params.delimiter)

    var pingSocket: Socket = null
    var out: PrintWriter = null
    var in: BufferedReader = null
    try {
        pingSocket = new Socket(params.socketHost, params.socketPort)
        out = new PrintWriter(pingSocket.getOutputStream, true)
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream))

        List.range(0, params.recordsNumber).foreach(_=>{
          out.println(
            eventCSVMapper.toCsv(eventGenerator.generateEvent())
          )
          System.out.println(in.readLine)
        })

        out.close()
        in.close()
        pingSocket.close()
      } catch {
        case e: IOException => println("error: " + e)
      }
    }

}
