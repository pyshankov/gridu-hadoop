package com.griddynamics.pmarchenko.labs.hadoop.spark.util

import java.util.{ PriorityQueue => JavaPriorityQueue}
import scala.collection.JavaConverters._
import scala.collection.generic.Growable

class BoundedPriorityQueue[A](maxSize: Int)(implicit ord: Ordering[A]) extends Iterable[A] with Growable[A] with Serializable {

  private val javaPriorityQueueDelegate = new JavaPriorityQueue[A](maxSize, ord)

  override def iterator: Iterator[A] = javaPriorityQueueDelegate.iterator.asScala

  override def size: Int = javaPriorityQueueDelegate.size

  override def ++=(xs: TraversableOnce[A]): this.type = {
    xs.foreach { this += _ }
    this
  }

  override def +=(elem: A): this.type = {
    if (size < maxSize) {
      javaPriorityQueueDelegate.offer(elem)
    } else {
      maybeReplaceLowest(elem)
    }
    this
  }

  def poll(): A = javaPriorityQueueDelegate.poll()

  override def +=(elem1: A, elem2: A, elems: A*): this.type = this += elem1 += elem2 ++= elems

  override def clear(): Unit = javaPriorityQueueDelegate.clear()

  private def maybeReplaceLowest(a: A): Boolean = {
    val head = javaPriorityQueueDelegate.peek()
    if (head != null && ord.gt(a, head)) {
      javaPriorityQueueDelegate.poll()
      javaPriorityQueueDelegate.offer(a)
    } else {
      false
    }
  }
}