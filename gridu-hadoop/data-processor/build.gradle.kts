plugins {
    java
    scala
}

group = GriduHadoopDeps.baseGroup
version = GriduHadoopDeps.version

val mainClassDataGenerator: String = "${group}.generator.NetCatDataStream"
val mainClassSparkJob: String = "${group}.spark.SparkMain"

repositories {
    mavenCentral()
    maven {
        url = uri("https://repo.hortonworks.com/content/repositories/releases")
    }
    maven {
        url = uri("https://repo.spring.io/plugins-release")
    }
}

dependencies {
    compile(project(":common"))
    implementation(GriduHadoopDeps.Dependencies.scalaLib)
    implementation(GriduHadoopDeps.Dependencies.faker)
    implementation(GriduHadoopDeps.Dependencies.commonsMath)

    compileOnly(GriduHadoopDeps.Dependencies.sparkSql)
    compileOnly(GriduHadoopDeps.Dependencies.sparkCore)
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.register<Jar>("fatJarDataGenerator") {
    archiveAppendix.set("DataGen")
    manifest {
        attributes["Main-Class"] = mainClassDataGenerator // fully qualified class name of default main class
    }
    from(Callable { configurations["runtimeClasspath"].map { if (it.isDirectory) it else zipTree(it) } })
    with(tasks["jar"] as CopySpec)
}

tasks.register<Jar>("jarSparkJob") {
    archiveAppendix.set("SparkJob")
    manifest {
        attributes["Main-Class"] = mainClassSparkJob // fully qualified class name of default main class
    }
    from(Callable { configurations["runtimeClasspath"].map { if (it.isDirectory) it else zipTree(it) } })
    with(tasks["jar"] as CopySpec)
}