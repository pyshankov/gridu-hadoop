export NC_HOST=172.18.0.2|sandbox-hdp.hortonworks.com
export NC_PORT=60080
export BASE_PATH=/user/root/lab_gridu_hadoop

#Start flume on HDP
/bin/flume-ng agent --conf ~/lab_gridu_hadoop/flume/flume-conf \
      -f ~/lab_gridu_hadoop/flume/flume-conf/flume-socket-hdfs.conf conf --name a1 \
     -Dflume.root.logger=INFO,console \
     -DpropertiesImplementation=org.apache.flume.node.EnvVarResolverProperties

#Run localy to send data
java -jar  /root/lab_gridu_hadoop/data-processor-DataGen-1.0.jar ${NC_HOST} ${NC_PORT} 2020-01-01 2020-01-07 1000 ,