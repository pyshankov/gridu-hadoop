export SPARK_PARALLELISM=2
export DB_DRIVER_DIR=/root/lab_gridu_hadoop/postgresql-9.2-1002.jdbc4.jar
export DB_JBDC_URL=jdbc:postgresql://127.0.0.1/analytics
export DB_DRIVER_CLASS=org.postgresql.Driver
export DB_USER=ambari
export DB_PASSWORD=bigdata
export JOB_FILE_PATH=/root/lab_gridu_hadoop/data-processor-SparkJob-1.0.jar

#  top 10 products within category
/bin/spark-submit \
  --conf spark.default.parallelism=${SPARK_PARALLELISM} \
  --conf spark.sql.shuffle.partitions=${SPARK_PARALLELISM} \
  --master yarn \
  --deploy-mode client \
  --jars ${DB_DRIVER_DIR} \
  ${JOB_FILE_PATH} \
  lab_gridu_hadoop/events/*/*/* \
  lab_gridu_hadoop/blocks_ip4 \
  lab_gridu_hadoop/country_locations \
  Top10ProductsInCategory \
  ${DB_JBDC_URL} \
  ${DB_USER} \
  ${DB_PASSWORD} \
  public.top_10_products_each_category_spark_dataset \
  ${DB_DRIVER_CLASS} \
  Dataset

#  top 10 categories
/bin/spark-submit \
  --conf spark.default.parallelism=${SPARK_PARALLELISM} \
  --conf spark.sql.shuffle.partitions=${SPARK_PARALLELISM} \
  --master yarn \
  --deploy-mode client \
  --jars ${DB_DRIVER_DIR} \
  ${JOB_FILE_PATH} \
  lab_gridu_hadoop/events/*/*/* \
  lab_gridu_hadoop/blocks_ip4 \
  lab_gridu_hadoop/country_locations \
  Top10Categories \
  ${DB_JBDC_URL} \
  ${DB_USER} \
  ${DB_PASSWORD} \
  public.top_categories_spark_dataset \
  ${DB_DRIVER_CLASS} \
  Dataset

# top 10 coutries by money spending
/bin/spark-submit \
  --conf spark.default.parallelism=${SPARK_PARALLELISM} \
  --conf spark.sql.shuffle.partitions=${SPARK_PARALLELISM} \
  --master yarn \
  --deploy-mode client \
  --jars ${DB_DRIVER_DIR} \
  ${JOB_FILE_PATH} \
  lab_gridu_hadoop/events/*/*/* \
  lab_gridu_hadoop/blocks_ip4 \
  lab_gridu_hadoop/country_locations \
  Top10SpendingCountries \
  ${DB_JBDC_URL} \
  ${DB_USER} \
  ${DB_PASSWORD} \
  public.top_10_money_spending_countries_spark_dataset \
  ${DB_DRIVER_CLASS} \
  Dataset

