CREATE DATABASE analytics;
use analytics;

CREATE TABLE public.top_categories (
 product_category TEXT PRIMARY KEY,
 purchases_count integer
);

CREATE TABLE public.top_10_products_each_category (
        product_category TEXT ,
        product_name TEXT,
        purchases_count integer,
        PRIMARY KEY(product_category, product_name)
);

CREATE TABLE public.top_10_money_spending_countries (
 country TEXT PRIMARY KEY,
 amount DECIMAL
);