

set hive.variable.substitute=true;
-- set base_path=${hiveconf:base_path}; /user/root/lab_gridu_hadoop

------------------------------------------------------------------------------------------------------------------------
-- create database
------------------------------------------------------------------------------------------------------------------------
CREATE schema if not exists lab_gridu_hadoop;
use lab_gridu_hadoop;
------------------------------------------------------------------------------------------------------------------------
-- create external table for data provisioning
------------------------------------------------------------------------------------------------------------------------
CREATE EXTERNAL TABLE IF NOT EXISTS lab_gridu_hadoop.events (
  product_name STRING,
   product_price DOUBLE,
 date_time timestamp,
 product_category STRING,
 source_ip STRING
)
 PARTITIONED BY(hday STRING)
 row format delimited
 fields terminated by ','
 stored as TEXTFILE
 LOCATION '${hiveconf:base_path}/events';

------------------------------------------------------------------------------------------------------------------------
-- create managed table for complex sql queries
------------------------------------------------------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS lab_gridu_hadoop.events_orc (
  product_name STRING,
   product_price DOUBLE,
 date_time timestamp,
 product_category STRING,
 source_ip STRING
)
 PARTITIONED BY(hday STRING)
 clustered by (product_category) sorted by (date_time) into 4 buckets
 stored as ORC;
------------------------------------------------------------------------------------------------------------------------
-- top_categories result table
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS lab_gridu_hadoop.top_categories (
 product_category STRING,
 purchases_count INT
)
stored as ORC;
------------------------------------------------------------------------------------------------------------------------
-- top_10_products_each_category result table
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS lab_gridu_hadoop.top_10_products_each_category (
 product_category STRING,
 product_name STRING,
 purchases_count INT
)
stored as ORC;
------------------------------------------------------------------------------------------------------------------------
-- ip table list
------------------------------------------------------------------------------------------------------------------------
CREATE EXTERNAL TABLE IF NOT EXISTS lab_gridu_hadoop.blocks_ip4 (
 network STRING,
 geoname_id BIGINT,
 registered_country_geoname_id BIGINT,
 represented_country_geoname_id BIGINT,
 is_anonymous_proxy BOOLEAN,
 is_satellite_provider BOOLEAN
)
ROW FORMAT serde 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
LOCATION '${hiveconf:base_path}/blocks_ip4'
tblproperties ("skip.header.line.count"="1");
------------------------------------------------------------------------------------------------------------------------
-- country locations
------------------------------------------------------------------------------------------------------------------------
CREATE EXTERNAL TABLE IF NOT EXISTS lab_gridu_hadoop.country_locations (
 geoname_id BIGINT,
 locale_code STRING,
 continent_code STRING,
 continent_name STRING,
 country_iso_code STRING,
 country_name STRING,
 is_in_european_union BOOLEAN
)
ROW FORMAT serde 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
LOCATION '${hiveconf:base_path}/country_locations'
tblproperties ("skip.header.line.count"="1");
------------------------------------------------------------------------------------------------------------------------
-- top 10 money spending countries
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS lab_gridu_hadoop.top_10_money_spending_countries (
 country STRING,
 amount DOUBLE
)
stored as ORC;