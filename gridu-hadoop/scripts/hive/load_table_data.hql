------------------------------------------------------------------------------------------------------------------------
-- create database
------------------------------------------------------------------------------------------------------------------------
CREATE schema if not exists lab_gridu_hadoop;
use lab_gridu_hadoop;
-- set base_path=${hiveconf:base_path}; /user/root/lab_gridu_hadoop
-- set udf_path=${hiveconf:udf_path}; /root/lab_gridu_hadoop/hive-udf-1.0.jar
------------------------------------------------------------------------------------------------------------------------
 -- apply loaded by flume data to external table (provision previous day data when a new day starts)
------------------------------------------------------------------------------------------------------------------------
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-01') location '${hiveconf:base_path}/events/2020/01/01';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-02') location '${hiveconf:base_path}/events/2020/01/02';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-03') location '${hiveconf:base_path}/events/2020/01/03';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-04') location '${hiveconf:base_path}/events/2020/01/04';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-05') location '${hiveconf:base_path}/events/2020/01/05';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-06') location '${hiveconf:base_path}/events/2020/01/06';
ALTER TABLE lab_gridu_hadoop.events add partition (hday='2020-01-07') location '${hiveconf:base_path}/events/2020/01/07';
------------------------------------------------------------------------------------------------------------------------
-- load data from tmp table to orc for analytics
------------------------------------------------------------------------------------------------------------------------
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-01')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-01';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-02')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-02';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-03')
   SELECT product_name, product_price, date_time, product_category, source_ip
   FROM lab_gridu_hadoop.events e where e.hday = '2020-01-03';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-04')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-04';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-05')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-05';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-06')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-06';
INSERT INTO TABLE lab_gridu_hadoop.events_orc PARTITION (hday = '2020-01-07')
   SELECT product_name, product_price, date_time, product_category, source_ip
  FROM lab_gridu_hadoop.events e where e.hday = '2020-01-07';
------------------------------------------------------------------------------------------------------------------------
-- calculate top 10 categories
------------------------------------------------------------------------------------------------------------------------
 INSERT OVERWRITE TABLE lab_gridu_hadoop.top_categories
    SELECT e.product_category, count(*) as purchases_count
       FROM lab_gridu_hadoop.events_orc e
      group by e.product_category
      ORDER BY  purchases_count DESC LIMIT 10;
------------------------------------------------------------------------------------------------------------------------
-- calculate top 10 products for each categories
------------------------------------------------------------------------------------------------------------------------
INSERT OVERWRITE TABLE lab_gridu_hadoop.top_10_products_each_category
    SELECT product_category, product_name, purchases_count FROM
       (SELECT product_category, product_name, purchases_count,
       rank() over (PARTITION BY product_category ORDER BY purchases_count DESC) as rank
       FROM
        (SELECT  e.product_category, e.product_name, count(*) as purchases_count
          from lab_gridu_hadoop.events_orc e
       	 group by e.product_category, e.product_name) gr) ranked where rank <= 10;
------------------------------------------------------------------------------------------------------------------------
-- calculate top 10 countries by money spending
------------------------------------------------------------------------------------------------------------------------
 -- write down an udf function in order to test and join IP within a network

 ADD JAR ${hiveconf:udf_path};
 CREATE TEMPORARY FUNCTION IPMATCH AS 'com.griddynamics.pmarchenko.labs.hadoop.udf.IPWithinNetworkMaskUDF';

 set hive.cbo.enable=true;
 set hive.execution.engine=tez;

INSERT OVERWRITE TABLE lab_gridu_hadoop.top_10_money_spending_countries
select cl.country_name as country, sum(tmp.product_price) as amount
    from (select e.source_ip, ip.network, ip.geoname_id, e.product_price from lab_gridu_hadoop.events_orc e
            cross join lab_gridu_hadoop.blocks_ip4 ip  where IPMATCH(e.source_ip, ip.network) == true) tmp
inner join lab_gridu_hadoop.country_locations cl on cl.geoname_id = tmp.geoname_id
    GROUP BY cl.country_name order by amount desc LIMIT 10;