export HDFS_DATA_PATH=/user/root/lab_gridu_hadoop
export UDF_PATH=/root/lab_gridu_hadoop/hive/hive-udf-1.0.jar

hive --hiveconf base_path=${HDFS_DATA_PATH} \
     -f /root/lab_gridu_hadoop/hive/create_tables.hql

hive --hiveconf base_path=${HDFS_DATA_PATH} \
     --hiveconf udf_path=${UDF_PATH} \
     -f /root/lab_gridu_hadoop/hive/load_table_data.hql