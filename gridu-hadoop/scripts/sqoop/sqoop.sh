export DB_DRIVER_DIR=/root/lab_gridu_hadoop/postgresql-9.2-1002.jdbc4.jar
export DB_JBDC_URL=jdbc:postgresql://127.0.0.1/analytics
export DB_DRIVER_CLASS=org.postgresql.Driver
export DB_USER=ambari
export DB_PASSWORD=bigdata

# load top 10 categories hive
sqoop export --connect ${DB_JBDC_URL} \
    --driver ${DB_DRIVER_CLASS} \
    --username ${DB_USER} \
    --password ${DB_PASSWORD} \
    --table public.top_categories \
    --hcatalog-table top_categories \
    --hcatalog-database lab_gridu_hadoop \
    --update-key product_category \
    --verbose

# clear data for load
sqoop export --connect ${DB_JBDC_URL} \
    --driver ${DB_DRIVER_CLASS} \
    --username ${DB_USER} \
    --password ${DB_PASSWORD} \
    --query "TRUNCATE TABLE public.top_10_products_each_category"

# load top 10 products within category
sqoop export --connect ${DB_JBDC_URL} \
    --driver ${DB_DRIVER_CLASS} \
    --username ${DB_USER} \
    --password ${DB_PASSWORD} \
    --table public.top_10_products_each_category \
    --hcatalog-table top_10_products_each_category \
    --hcatalog-database lab_gridu_hadoop \
    --verbose

# load top 10 countries by money spending
sqoop export --connect ${DB_JBDC_URL} \
    --driver ${DB_DRIVER_CLASS} \
    --username ${DB_USER} \
    --password ${DB_PASSWORD} \
    --table public.top_10_money_spending_countries \
    --hcatalog-table top_10_money_spending_countries \
    --hcatalog-database lab_gridu_hadoop \
    --verbose
