package com.griddynamics.pmarchenko.labs.hadoop.common;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class IpAddressMatcher {

    private static InetAddress parseAddress(String address) {
        try {
            return InetAddress.getByName(address);
        }
        catch (UnknownHostException e) {
            throw new IllegalArgumentException("Failed to parse address" + address, e);
        }
    }

    public static boolean isIpWithinMask(String ip, String mask){
        int nMaskBits;
        InetAddress requiredAddress;

        if (mask.indexOf('/') > 0) {
            String[] addressAndMask = mask.split("/");
            mask = addressAndMask[0];
            nMaskBits = Integer.parseInt(addressAndMask[1]);
        }
        else {
            nMaskBits = -1;
        }
        requiredAddress = parseAddress(mask);
        assert  (requiredAddress.getAddress().length * 8 >= nMaskBits) :
                String.format("IP address %s is too short for bitmask of length %d",
                        mask, nMaskBits);

        InetAddress remoteAddress = parseAddress(ip);
        if (!requiredAddress.getClass().equals(remoteAddress.getClass())) {
            return false;
        }
        if (nMaskBits < 0) {
            return remoteAddress.equals(requiredAddress);
        }
        byte[] remAddr = remoteAddress.getAddress();
        byte[] reqAddr = requiredAddress.getAddress();
        int nMaskFullBytes = nMaskBits / 8;
        byte finalByte = (byte) (0xFF00 >> (nMaskBits & 0x07));
        for (int i = 0; i < nMaskFullBytes; i++) {
            if (remAddr[i] != reqAddr[i]) {
                return false;
            }
        }
        if (finalByte != 0) {
            return (remAddr[nMaskFullBytes] & finalByte) == (reqAddr[nMaskFullBytes] & finalByte);
        }
        return true;
    }


}