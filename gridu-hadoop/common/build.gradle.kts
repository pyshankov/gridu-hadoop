plugins {
    java
    `java-library`
}

group = GriduHadoopDeps.extendGroup("common")
version = GriduHadoopDeps.version

repositories {
    mavenCentral()
}

dependencies {
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
