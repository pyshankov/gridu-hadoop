rootProject.name = "gridu-labs"

include(
        ":data-processor",
        ":hive-udf",
        ":common"
)

project(":data-processor").projectDir = file("gridu-hadoop/data-processor")
project(":hive-udf").projectDir = file("gridu-hadoop/hive-udf")
project(":common").projectDir = file("gridu-hadoop/common")

