object GriduHadoopDeps {

    val baseGroup: String = "com.griddynamics.pmarchenko.labs.hadoop"
    val version: String = "1.0"

    fun extendGroup(module: String) = "${baseGroup}.${module}"

    object Dependencies {
        val scalaVersion: String = "2.11"
        val sparkVersion: String = "2.3.0"
        val hdpVersion: String = "2.6.5.0-292"
        //scala
        val scalaLib: String = "org.scala-lang:scala-library:${scalaVersion}.12"
        //java
        val commonsMath: String = "org.apache.commons:commons-math3:3.6.1"
        val faker: String = "com.github.javafaker:javafaker:1.0.1"
        //spark
        val sparkSql: String = "org.apache.spark:spark-sql_${scalaVersion}:${sparkVersion}.${hdpVersion}"
        val sparkCore: String = "org.apache.spark:spark-core_${scalaVersion}:${sparkVersion}.${hdpVersion}"
        //hadoop
        val hadoopClient: String = "org.apache.hadoop:hadoop-client:2.7.3.${hdpVersion}"
        val hiveExec: String = "org.apache.hive:hive-exec:1.2.1000.${hdpVersion}"
    }
}

